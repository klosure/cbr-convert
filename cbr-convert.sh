#!/bin/sh
#
# author: klosure
# https://gitlab.com/klosure/cbr-convert
# license: isc (SEE LICENSE.md)
#
# --- Convert comic book files from .cbr to .cbz ---
# The .cbr (comic book rar) file format is proprietary and doesn't play well
# with all comic book readers. This script will recurse a directory and convert
# each .cbr file found to .cbz
#
# Dependencies: trash-cli, zip, unar, gnu parallel


VERSION="0.1.2"
# Portable way to get the number of cores available to the system (Linux, BSD, and MacOS)
CORES_AVAIL=$(getconf _NPROCESSORS_ONLN 2>/dev/null || sysctl -n hw.ncpu || echo 2)
# Default number of threads to use (half available cores)
JOBS=$(( CORES_AVAIL / 2 ))
LOGDIR=~/.cbr-convert
LOGFILE=$LOGDIR/errors.log


trap fun_cleanup INT


fun_echo_dirs () {
    # Simple and POSIX
    find "$@" -type d -exec echo {} \;
}

# call this function to start the script
fun_run () {
    fun_echo_dirs "$@" | parallel -j $JOBS runner-cbr "secretargument" {}
}


# log that an error occurred
fun_log_error () {
    if ! [ -d $LOGDIR ] ; then
	mkdir $LOGDIR
    fi
    echo "$(date): $1" | tee -a $LOGFILE
    echo "(logged @ $LOGFILE)"
}


# pass the error message, we echo it, log it, and print the help info
fun_error () {
    fun_log_error "$1"
    echo ""
    fun_print_usage
    exit 1
}


# run when ctrl-c is pressed
fun_cleanup () {
    echo "caught sigint, exiting.."
    exit 0
}


fun_print_usage () {
    echo "usage:" 
    echo "       cbr-convert ./MyComicsDir    # convert all cbr files found in this directory (and subdirectories)"
    echo "       cbr-convert -j 8 ./Deadpool  # convert using 8 threads"
    echo "       cbr-convert -h               # show this help message"
    echo "       cbr-convert -v               # show version"
    echo ""
    echo "( After conversion .cbr files are moved to trash )"
}


fun_print_info () {
    echo "cbr-convert - .cbr comic book files to .cbz"
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/cbr-convert"
    echo "license: isc"
    fun_print_ver
    echo ""
    fun_print_usage
}


fun_print_ver () {
    echo "cbr-convert version: $VERSION"
}


# checks if we were passed a valid dir
fun_check_dir () {
    if [ "$#" -gt 0 ] && [ -d "$*" ] && [ "$(ls -A "$@")" ] ; then
	echo "Starting up cbr-convert with $JOBS jobs..."
	fun_run "$@"
    else
	fun_error "Please pass in a valid (i.e. non-empty) directory"
    fi
}


# set the number of jobs (threads) to spawn
fun_set_numjobs () {
    if [ "$1" = "-j" ] ; then
	JOBS="$2"
	shift; shift
	fun_check_dir "$@"
    else
	fun_error "Bad State! - fun_set_numjobs"
    fi
}


fun_check_for_errors () {
    if [ -s $LOGFILE ] ; then
	echo "Potential errors detected!"
	echo "Please check file $LOGFILE to verify"
    fi
}

fun_clear_old_log () {
    if [ -s $LOGFILE ] ; then
	trash-put $LOGFILE
    fi
}

# parse what arguments were passed in
# call a corresponding function
fun_check_args () {
    if [ "$#" -ge 1 ] ; then
	case "$1" in
	    -j)  fun_set_numjobs "$@" ;;
            -h)	 fun_print_info "$@" ;;
            -v)  fun_print_ver "$@" ;;
            *)	 fun_check_dir "$@" ;;
	esac
    else
	fun_error "Pass in a directory or flag!"
    fi
}

fun_check_deps () {
    if ! [ -x "$(command -v unar)" ]; then
	fun_error "error: cbr-convert needs unar, please install it."
    elif ! [ -x "$(command -v parallel)" ]; then
	fun_error "error: cbr-convert needs GNU parallel, please install it."
    elif ! [ -x "$(command -v trash-put)" ]; then
	fun_error "error: cbr-convert needs trash-cli, please install it."
    elif ! [ -x "$(command -v zip)" ]; then
	fun_error "error: cbr-convert needs zip, please install it."
    fi
}

# -- ENTRY POINT--
fun_check_deps
fun_clear_old_log
fun_check_args "$@"
fun_check_for_errors
exit 0
