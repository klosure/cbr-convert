#!/bin/sh
#
# author: klosure
# https://gitlab.com/klosure/cbr-convert
# license: isc
#
# Helper file for cbr-convert.sh
# DON'T CALL THIS FILE ON ITS OWN


LOGDIR=~/.cbr-convert
LOGFILE=$LOGDIR/errors.log

trap fun_catch_sigint INT

# check if cbr file
# cuts off extension and compares it
fun_is_cbr () {
    L_FILE="$1"
    # ignore directories
    if [ -f "$L_FILE" ] ; then
	L_FILE_EXT="${L_FILE##*.}"
	if [ "$L_FILE_EXT" = "cbr" ] ; then
	    unset L_FILE L_FILE_EXT
	    return 0    
	fi
	unset L_FILE_EXT
    fi
    unset L_FILE 
    return 1
}

# run when ctrl-c is pressed
fun_catch_sigint () {
    exit 0
}

fun_log_error () {
    if ! [ -d $LOGDIR ] ; then
	mkdir $LOGDIR
    fi
    echo "$(date): $1" | tee -a $LOGFILE
    exit 1
}

# were we called by cbr-convert?
if ! [ "$1" = "secretargument" ] ; then
    fun_log_error "Do not call this script on its own, use cbr-convert!"
elif [ "$#" -lt 2 ] ; then
    fun_log_error "You must pass a directory"
else
    shift
fi

# does the dir we were given exist?
if [ -d "$1" ] ; then
    cd "$1" || fun_log_error "Could not enter directory $1"
    echo "Working On: $1"
else
    fun_log_error "Passed a bad dir? : $1"
    exit 1
fi

CBRS_FOUND="no"
for FILE in * ; do
    [ -e "$FILE" ] || continue
    if fun_is_cbr "$FILE" ; then
	CBRS_FOUND="yes"
	echo "Converting: $FILE"
	DIR="${FILE%.*}"
	mkdir "$DIR"
	unar -quiet ./"$FILE" -o "$DIR" >> $LOGFILE 2>&1
	zip --quiet -r "$DIR".cbz "$DIR" >> $LOGFILE 2>&1
	rm -r "$DIR"
    fi
done # conversion complete

# put .cbr files into trash
if [ "$CBRS_FOUND" = "yes" ] ; then
    trash-put ./*.cbr
fi

exit 0

