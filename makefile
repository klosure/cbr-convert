PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./cbr-convert.sh $(BINDIR)/cbr-convert
	cp ./runner-cbr.sh $(BINDIR)/runner-cbr
	cp ./cbr-convert.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/cbr-convert
	rm $(BINDIR)/runner-cbr
	rm $(MANDIR)/cbr-convert.1
test:
	shellcheck cbr-convert.sh runner-cbr.sh
