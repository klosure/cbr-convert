# cbr-convert

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

### Motivation
The .cbr (comic book rar) file format is proprietary and doesn't play well with all comic book readers. This script will recurse a directory and convert each .cbr file found to .cbz. We divide up the work by directory and use gnu parallel to execute on each folder. After the work is complete we cleanup the old cbr files and move them to the trash folder. This tool is meant to be a one-shot to convert your whole collection. Spaces in directories or filenames are anticipated and OK 👍. 

### Dependencies
In order to use this tool, you will need these things installed (some are likely already present on your system):

+ [unar](https://theunarchiver.com/command-line)
+ [zip](https://www.digitalocean.com/community/questions/how-to-install-zip-in-ubuntu)
+ [trash-cli](https://github.com/andreafrancia/trash-cli)
+ [GNU Parallel](https://www.gnu.org/software/parallel)
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)
+ POSIX [shell](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)

### Installation
Simply run:

```bash
git clone https://gitlab.com/klosure/cbr-convert.git
cd cbr-convert
make install      #(as root)

# to uninstall
make uninstall    #(as root)
```

### Usage
```bash 
cbr-convert ./MyComicsDir    # convert all cbr files found in this directory (and subdirectories)
cbr-convert -j 8 ./Deadpool  # convert using 8 threads
cbr-convert -h               # show this help message
cbr-convert -v               # show version

( After conversion .cbr files are moved to trash )
```

### Video Example

[![asciicast](https://asciinema.org/a/Na7335fPiivYB3yRilK6NJzEL.svg)](https://asciinema.org/a/Na7335fPiivYB3yRilK6NJzEL)

### Benchmarks
```bash
Dataset = 22GB
CPU = 4th Gen i7 Quad Core
Storage = 250 Sata III SSD

-j 1 (sequential)      -j 8 (multithreaded)
real	18m34.91s     real	5m49.69s

Conclusion:
This tool by default uses half the available cores, if possible specify a higher -j value to help speed things up.
```

### TODO
- Change log location (`-l` flag)
- Silent mode (`-s` flag)
- Possibly disable recursion by default.

### License / Disclaimer
This project is licensed under the [ISC](https://opensource.org/licenses/ISC) license. (See LICENSE.md)

